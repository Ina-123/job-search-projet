import {setupWorker} from 'msw/browser';
import { mockHandlers } from './mocks';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';


setupWorker(...mockHandlers).start()
  .then(() => platformBrowserDynamic().bootstrapModule(AppModule))
  .catch((err) => console.error(err));

