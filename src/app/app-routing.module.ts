import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path : '', loadChildren : ()=> import('./job/job.module').then(m => m.JobModule) },
    { path : '**', pathMatch : 'full', redirectTo : ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
