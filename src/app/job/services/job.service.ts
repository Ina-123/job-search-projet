import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Job } from '../models/job.models';

@Injectable()

export class JobService {

  constructor(private httpClient : HttpClient) { }

  getJobs() : Observable<Job[]>{
    return this.httpClient.get<Job[]>(`/jobs`);
  }

  getJobById(jobId : number) : Observable<Job>{
    return this.httpClient.get<Job>(`/jobs/${jobId}`);
  }

  private _jobs$ = new BehaviorSubject<Job[]>([]);
  get jobs$() : Observable<Job[]>{
    return this._jobs$.asObservable();
  }

  loadedJobsData(jobs : Job[]){
    this._jobs$.next(jobs);
  }
}
