import { Component, OnInit } from '@angular/core';
import { Observable, catchError, delay, map, of, tap, throwError } from 'rxjs';
import { Job } from '../../models/job.models';
import { JobService } from '../../services/job.service';
import { JOBS } from '../../constants/jobs.constant';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrl: './job-list.component.css'
})
export class JobListComponent implements OnInit {
  jobs$! : Observable<Job[]>;
  jobList! : Job[];
  errorMessage! : string | null;

  constructor(private jobService : JobService){}

  ngOnInit(): void {
    this.initObservable();
  }

  private initObservable() : void{
    const storedJobs = localStorage.getItem(JOBS);

    if(storedJobs){
      this.jobList = JSON.parse(storedJobs);
      this.jobs$ = of(this.jobList);
      this.getFavoriteJobList(this.jobList);
    }else{
      this.errorMessage = null;
      this.jobs$ = this.jobService.getJobs().pipe(
        delay(1000),
        map((jobsData : Job[]) => jobsData.map((job: Job) => ({...job, isFavorite : false}))),
        tap((updatedJobsData : Job[]) => { this.jobList = updatedJobsData; localStorage.setItem(JOBS, JSON.stringify(this.jobList))}),
        catchError((error : string) =>{
          this.errorMessage = error;
          return throwError('error');
        })
      )
    }
    this.jobs$.subscribe();
  }

  OnJobFavoriteToggled(data : {id : number, isFavorite : boolean}) : void{
    if(!data?.id){
      return;
    }

    this.jobList = this.jobList.map((job : Job) => job.id === data.id ? {...job, isFavorite : data.isFavorite} : job);
    this.jobs$ = of(this.jobList);
    localStorage.setItem(JOBS, JSON.stringify(this.jobList));
    this.getFavoriteJobList(this.jobList);
  }

  private getFavoriteJobList(jobs : Job[]) : void{
    let favoriteJobs = jobs.filter((job : Job) => job.isFavorite);
    this.jobService.loadedJobsData(favoriteJobs);
  }
}
