import { Component, OnInit } from '@angular/core';
import { Job } from '../../models/job.models';
import { JobService } from '../../services/job.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-favourite-job-list',
  templateUrl: './favorite-job-list.component.html',
  styleUrl: './favorite-job-list.component.css'
})
export class FavoriteJobListComponent implements OnInit{
  favoriteJobs$! : Observable<Job[]>;

  constructor(private jobService : JobService){}

  ngOnInit(): void {
    this.initObservable();
  }
  
  private initObservable() : void{
    this.favoriteJobs$ = this.jobService.jobs$;
    this.favoriteJobs$.subscribe();
  }

}
