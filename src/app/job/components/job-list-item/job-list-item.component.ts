import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Job } from '../../models/job.models';

@Component({
  selector: 'app-job-list-item',
  templateUrl: './job-list-item.component.html',
  styleUrl: './job-list-item.component.css'
})
export class JobListItemComponent {
  @Input() job! : Job;
  @Input() showStarIcon! : boolean;
  @Output() jobFavoriteToggled = new EventEmitter<{id : number, isFavorite : boolean}>();

  toggleFavoriteStatus() : void {
    this.job.isFavorite = !this.job.isFavorite;
    this.jobFavoriteToggled.emit({id : this.job.id, isFavorite : this.job.isFavorite});
  }
}
